/**
 * User Interface JS.
 *
 * @package LocationTools\Location_Tools_UI
 * @author Lance Cleveland <lance@storelocatorplus.com>
 * @copyright 2016 Charleston Software Associates, LLC
 *
 * @type {LOCATION_TOOLS}
 */

// Setup the Location Tools namespace
var LOCATION_TOOLS = LOCATION_TOOLS || {};


/**
 * The Location List Class
 */
var location_tools_distance_calculator = function () {

    /**
     * Initialize the distance calculator if the form is on the page.
     */
    this.initialize = function() {
        if ( ! jQuery( '#lt_distance_calculator' ) ) { return; }
        jQuery( '#lt_distance_calculator input' ).change( LOCATION_TOOLS.distance_calculator.calculate_distance );
    }

    /**
     * Calculate the distance between the starting and ending coordinates.
     */
    this.calculate_distance = function () {
        var lat1 = jQuery( '#lt_dc_lat_start' ).val();
        var lng1 = jQuery( '#lt_dc_lng_start' ).val();
        var lat2 = jQuery( '#lt_dc_lat_end' ).val();
        var lng2 = jQuery( '#lt_dc_lng_end' ).val();

        // calculate distance, bearing, mid-point
        var p1 = lt_Coordinate(lat1, lng1);
        var p2 = lt_Coordinate(lat2, lng2);
        var dist = p1.distance_to(p2);

        // display results
        jQuery('#lt_dc_result').html((dist/1000).toPrecisionDecimal(4)); // in mi
    }
};

/**
 * A Coordinate Class
 *
 * Earth radius (average) is 6,371km or   3,959 mi
 */
function lt_Coordinate( lat , lng ) {
    if ( ! ( this instanceof lt_Coordinate ) ) return new lt_Coordinate( lat , lng );

    this.lat = Number(lat);
    this.lng = Number(lng);
}

/**
 * Calculate the bearing to a point from current coordinate.
 *
 * @param point
 *
 * @returns {number}
 */
lt_Coordinate.prototype.bearing_to = function(point) {
    if (!(point instanceof lt_Coordinate)) throw new TypeError('Destination is not a coordinate.');

    var φ1 = this.lat.toRadians(), φ2 = point.lat.toRadians();
    var Δλ = (point.lng-this.lng).toRadians();

    // see http://mathforum.org/library/drmath/view/55417.html
    var y = Math.sin(Δλ) * Math.cos(φ2);
    var x = Math.cos(φ1)*Math.sin(φ2) -
        Math.sin(φ1)*Math.cos(φ2)*Math.cos(Δλ);
    var θ = Math.atan2(y, x);

    return (θ.toDegrees()+360) % 360;
};

/**
 * Coordinate, distance between two points.
 *
 * @param pathStart
 * @param pathEnd
 * @param radius    defaults to earth radius in miles (3,959)
 * @returns {number}
 */
lt_Coordinate.prototype.distance_between_points = function( pathStart , pathEnd , radius ) {
    if ( ! ( pathStart instanceof lt_Coordinate ) ) throw new TypeError('Starting point is not a coordinate.');
    if ( ! ( pathEnd   instanceof lt_Coordinate ) ) throw new TypeError('Ending point is not a coordinate.');

    radius = (radius === undefined) ? 3959e3 : Number(radius);

    var δ13 = pathStart.distance_to(this, radius)/radius;
    var θ13 = pathStart.bearing_to(this).toRadians();
    var θ12 = pathStart.bearing_to(pathEnd).toRadians();

    var dxt = Math.asin( Math.sin(δ13) * Math.sin(θ13-θ12) ) * radius;

    return dxt;
};

/**
 * Coordinate distance to a point from current coordinate.
 *
 * @param point
 * @param radius
 * @returns {number}
 */
lt_Coordinate.prototype.distance_to = function( point , radius ) {
    if (!(point instanceof lt_Coordinate)) throw new TypeError('Destination is not a coordinate.');
    radius = (radius === undefined) ? 3959e3 : Number(radius);

    var R = radius;
    var φ1 = this.lat.toRadians(),  λ1 = this.lng.toRadians();
    var φ2 = point.lat.toRadians(), λ2 = point.lng.toRadians();
    var Δφ = φ2 - φ1;
    var Δλ = λ2 - λ1;

    var a = Math.sin(Δφ/2) * Math.sin(Δφ/2)
        + Math.cos(φ1) * Math.cos(φ2)
        * Math.sin(Δλ/2) * Math.sin(Δλ/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;

    return d;
};


// Document Ready
jQuery( document ).ready(
    function () {

        if (Number.prototype.toDegrees === undefined) {
            Number.prototype.toDegrees = function() { return this * 180 / Math.PI; };
        }

        if (Number.prototype.toRadians === undefined) {
            Number.prototype.toRadians = function() { return this * Math.PI / 180; };
        }

        if (typeof Number.prototype.toPrecisionDecimal == 'undefined') {
            Number.prototype.toPrecisionDecimal = function (precision) {

                // use standard toPrecision method
                var n = this.toPrecision(precision);

                // ... but replace +ve exponential format with trailing zeros
                n = n.replace(/(.+)e\+(.+)/, function (n, sig, exp) {
                    sig = sig.replace(/\./, '');       // remove decimal from significand
                    var l = sig.length - 1;
                    while (exp-- > l) sig = sig + '0'; // append zeros from exponent
                    return sig;
                });

                // ... and replace -ve exponential format with leading zeros
                n = n.replace(/(.+)e-(.+)/, function (n, sig, exp) {
                    sig = sig.replace(/\./, '');       // remove decimal from significand
                    while (exp-- > 1) sig = '0' + sig; // prepend zeros from exponent
                    return '0.' + sig;
                });

                return n;
            }
        }

        if (typeof module != 'undefined' && module.exports) module.exports = lt_Coordinate;

        LOCATION_TOOLS.distance_calculator = new location_tools_distance_calculator();
        LOCATION_TOOLS.distance_calculator.initialize();
    }
);
