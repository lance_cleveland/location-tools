<?php
if ( ! class_exists('LocationTools_BaseClass_Object') ) {

	/**
	 * Class LocationTools_BaseClass_Object
	 *
	 * @property-read	LocationTools		$addon;
	 *
	 * @package LocationTools\BaseClass\Object
	 * @author Lance Cleveland <lance@charlestonsw.com>
	 * @copyright 2016 Charleston Software Associates, LLC
	 */
	class LocationTools_BaseClass_Object {
		protected $addon;

		/**
		 * @param array $options
		 */
		function __construct( $options = array() ) {
			if ( is_array( $options ) && ! empty( $options ) ) {
				foreach ( $options as $property => $value ) {
					if ( property_exists( $this, $property ) ) {
						$this->$property = $value;
					}
				}
			}
			$this->addon = LocationTools::init();

			$this->initialize();
		}

		/**
		 * Do these things when this object is invoked.
		 */
		protected function initialize() {
			// Override with anything you want to run when your extension is invoked.
		}
	}

}