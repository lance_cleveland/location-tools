<?php

/**
 * Holds the ui-only code.
 *
 * @package LocationTools\UI
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2016 Charleston Software Associates, LLC
 *
 * @property-read   LocationTools_Shortcode $location_tools_shortcode
 */
class LocationTools_UI  extends LocationTools_BaseClass_Object {
    private $location_tools_shortcode;

    /**
     * Things we do when starting the UI.
     */
    public function initialize() {
        add_shortcode( 'location_tools' , array( $this , 'process_location_tools_shortcode' ) );
    }

    /**
     * Load up the code that handles [location_tools ...]
     *
     * @param $attributes
     */
    private function create_object_location_tools_shortcode( $attributes ) {
        if ( ! isset ( $this->location_tools_shortcode ) ) {
            require_once( 'class.shortcode.location_tools.php' );
            $this->location_tools_shortcode = new LocationTools_Shortcode();
            $this->enqueue_ui_javascript();
            $this->enqueue_ui_css();
        }
    }

    /**
     * If the file userinterface.css exists, enqueue it.
     */
    function enqueue_ui_css() {
        $filename = 'css/location_tools_ui.css';
        if ( file_exists( $this->addon->dir . $filename ) ) {
            wp_enqueue_style( $this->addon->short_slug . '_userinterface_css' , $this->addon->url . '/' . $filename );
        }
    }

    /**
     * If the file userinterface.js , enqueue it.
     */
    private function enqueue_ui_javascript() {
        $filename = 'js/location_tools_ui.js';
        if ( file_exists( $this->addon->dir . $filename ) ) {
            wp_enqueue_script( $this->addon->short_slug . '_userinterface' , $this->addon->url . '/' . $filename , array( 'jquery' ) );
        }
    }

    /**
     * Handle the location_tools shortcode.
     *
     * @param $attributes
     */
    public function process_location_tools_shortcode( $attributes ) {
        $this->create_object_location_tools_shortcode( $attributes );
        return $this->location_tools_shortcode->get_output();
    }
}

