<?php

/**
 * Holds the [location_tools] shortcode.
 *
 * @package LocationTools\Shortcode
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2016 Charleston Software Associates, LLC
 *
 * @property        string  $tool           Tool as specified in shortcode [slp_location tool='...']
 * @property-read   string  $output
 */
class LocationTools_Shortcode  extends LocationTools_BaseClass_Object {
    public $tool = 'distance_calculator';
    private $output = '';

    /**
     * Things we do at startup, like run the output for the specified tool.
     */
    public function initialize() {
        switch ( $this->tool ) {
            case 'distance_calculator':
                $this->distance_calculator_output();
                break;
        }
    }

    /**              
     * Get the shortcode output.
     * 
     * @return string
     */
    public function get_output() {
        return $this->output;
    }

    /**
     * Render distance calculator [location_tools tool='distance_calculator']
     */
    private function distance_calculator_output() {
        $label_start    = __( 'Starting Lat, Lng'     , 'location-tools' );
        $label_end      = __( 'Ending Lat, Lng'       , 'location-tools' );
        $label_distance = __( 'Distance: '            , 'location-tools' );
        $label_unit     = __( 'miles'                 , 'location-tools' );

        $this->output = "
        <div id='lt_distance_calculator' class='location_tools'>
            <div class='coordinates start'>
                <span class='location_tools input'>
                    <label for='lt_dc_lat_start'>{$label_start}</label>
                    <input id='lt_dc_lat_start' name='lt_dc_lat_start' class='lat_input'>
                </span>
                <span class='location_tools input'>
                    <span class='coordinate_separator'>,</span>
                    <input id='lt_dc_lng_start' name='lt_dc_lng_start' class='lng_input'>
                </span>
            </div>
            <div class='coordinates end'>
                <span class='location_tools input'>
                    <label for='lt_dc_lat_end'>{$label_end}</label>
                    <input id='lt_dc_lat_end' name='lt_dc_lat_end' class='lat_input'>
                </span>
                <span class='location_tools input'>
                    <span class='coordinate_separator'>,</span>
                    <input id='lt_dc_lng_end' name='lt_dc_lng_end' class='lng_input'>
                </span>
            </div>
            <div class='calculation output'>
                {$label_distance}<span id='lt_dc_result'></span><span id='lt_dc_unit'>{$label_unit}</span>
            </div>
        </div>
        ";
    }

}

