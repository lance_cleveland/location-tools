<?php

/**
 * Location Tools
 *
 * @package   LocationTools
 * @author    Lance Cleveland <lance@lancecleveland.com>
 * @copyright 2016 Charleston Software Associates, LLC
 *
 * @property        string              $dir
 * @property        LocationTools       $instance
 * @property        array               $options
 * @property-read   boolean             $options_set
 * @property        string              $short_slug     The short slug name (just the base no .php or dir)
 * @property        string              $slug
 * @property        LocationTools_UI    $ui
 * @property        string              $url
 */
class LocationTools {
    public  $dir;
    public  $instance;
    public  $options = array(
        'installed_version'       => '0.0',
    );
    private $options_set = false;
    public  $short_slug;
    public  $slug;
    private $ui;
    public  $url;

    /**
     * Constructor.
     */
    function __construct() {
        $this->url  = plugins_url( '', LOCATION_TOOLS__PLUGIN_FILE );
        $this->dir  = LOCATION_TOOLS__PLUGIN_DIR;
        $this->slug = plugin_basename( LOCATION_TOOLS__PLUGIN_FILE );
        $this->short_slug = $this->get_short_slug();
        require_once( 'base_class.object.php' );

        add_action( 'wp_enqueue_scripts', array( $this, 'createobject_UI' ) );
    }

    /**
     * Create and attach the activation processing object.
     */
    function createobject_Activation() {
        if ( ! isset ( $this->Activation ) ) {
            require_once( 'class.activation.php' );
            $this->Activation = new LocationTools_Activation();
        }
    }

    /**
     * Create and attach the UI processing object.
     */
    function createobject_UI() {
        if ( ! isset ( $this->ui ) ) {
            require_once( 'class.ui.php' );
            $this->ui = new LocationTools_UI();
        }
    }

    /**
     * Get the short slug, just the base/directory part of the fully qualified WP slug for this plugin.
     *
     * @return string
     */
    private function get_short_slug() {
        $slug_parts = explode('/', $this->slug);
        return str_replace('.php', '', $slug_parts[count($slug_parts) - 1]);
    }

    /**
     * Invoke the plugin as singleton.
     *
     * @static
     */
    public static function init() {
        static $instance = false;
        if ( ! $instance ) {
            load_plugin_textdomain( 'location-tools', false, dirname( plugin_basename( LOCATION_TOOLS__PLUGIN_FILE ) ) . '/languages/' );
            $instance = new LocationTools();
        }

        return $instance;
    }

    /**
     * Activate or update this plugin.
     */
    public static function plugin_activation() {
        $instance = LocationTools::init();
        $instance->set_options();
        if ( ! isset( $instance->options['installed_version'] ) || empty( $instance->options['installed_version'] ) ||
             version_compare( $instance->options['installed_version'], LOCATION_TOOLS__VERSION, '<' )
        ) {
            $instance->createobject_Activation();
        }
    }

    /**
     * Set the options by merging those from the DB with the defaults for this add-on pack.
     */
    function set_options() {
        if ( ! $this->options_set ) {
            $this->options     = array_merge( $this->options, get_option( 'locationtools_options', array() ) );
            $this->options_set = true;
        }
    }

}

