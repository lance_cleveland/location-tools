<?php
if (! class_exists('LocationTools_Activation')) {

    /**
     * Holds the activation code.
     *
     * @package LocationTools\Activation
     * @author Lance Cleveland <lance@charlestonsw.com>
     * @copyright 2016 Charleston Software Associates, LLC
     */
    class LocationTools_Activation extends LocationTools_BaseClass_Object {

        /**
         * @param array $options
         */
        function __construct( $options = array() ) {
            parent::__construct( $options );
            $this->update();
        }

        /**
         * Update this plugin.
         */
        private function update() {
            $this->addon->options['installed_version'] = LOCATION_TOOLS__VERSION;
            update_option( 'locationtools_options' , $this->addon->options );
        }
	}
}
