=== Location Tools ===
Plugin Name:  Location Tools
Contributors: charlestonsw
Donate link: https://www.storelocatorplus.com/product/location-tools/
Tags: map, locations, latitude, longitude, calculator
Requires at least: 4.5
Tested up to: 4.5.2
Stable tag: 4.5-RC-02

A set of generic global coordinate and mapping tools for your WordPress site.

== Description ==

A set of basic world map and coordinate tools you can place on your website with shortcodes.

= [location_tools tool="distance_calculator"] =

Render a form for users to enter a pair of latitude/longitude coordinates.  Calculates the distance between them whenever the coordinates are changed.



== Installation ==

= Requirements =

* Wordpress: 4.4
* PHP: 5.2.4

== Frequently Asked Questions ==

= What are the terms of the license? =

Like ALL of my plugins, including my premium add-ons, the license is GPL.
You get the code, feel free to modify it as you wish.
I prefer customers pay me because they like what I do and want to support my efforts to bring useful software to market.
Learn more on the  License Terms](https://www.storelocatorplus.com/products/general-eula/) page.

== Changelog ==

= 4.5 =

Initial release.
