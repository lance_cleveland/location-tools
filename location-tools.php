<?php
/**
 * Plugin Name: Location Tools
 * Plugin URI: https://www.storelocatorplus.com/product/location-tools/
 * Description: A set of generic global coordinate and mapping tools for your WordPress site.
 * Author: Charleston Software Associates
 * Author URI: http://charlestonsw.com/
 * Requires at least: 4.4
 * Tested up to : 4.5.2
 * Version: 4.5-RC-02
 *
 * Text Domain: location-tools
 * Domain Path: /languages/
 */

// Check WP Version
//
global $wp_version;
if ( version_compare( $wp_version, '4.4' , '<' ) ) {
    add_action(
        'admin_notices',
        create_function(
            '',
            "echo '<div class=\"error\"><p>".
            __( 'Location Tools requires WordPress 4.4 to function properly. '    , 'location-tools' ) ,
            __( 'This plugin has been deactivated.'                               , 'location-tools' ) .
            __( 'Please upgrade WordPress.'                                       , 'location-tools' ) .
            "</p></div>';"
        )
    );
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    deactivate_plugins( plugin_basename( __FILE__ ) );
    return;
}

define( 'LOCATION_TOOLS__VERSION'     ,   '4.5'                       );
define( 'LOCATION_TOOLS__PLUGIN_DIR'  ,   plugin_dir_path( __FILE__ ) );
define( 'LOCATION_TOOLS__PLUGIN_FILE' ,   __FILE__                    );

require_once( LOCATION_TOOLS__PLUGIN_DIR . 'include/class.location-tools.php' );

register_activation_hook( LOCATION_TOOLS__PLUGIN_FILE , array( 'LocationTools' , 'plugin_activation' ) );

add_action( 'init' , array( 'LocationTools' , 'init' ) );
